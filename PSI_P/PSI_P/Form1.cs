﻿using AForge.Neuro;
using AForge.Neuro.Learning;
using PSI_P.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PSI_P
{
    public partial class Form1 : Form
    {
        ActivationNetwork network;
        Image x, o;
        double[] spots = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        double[][] input;
        double[][] output;
        bool playerTurnB;

        public Form1()
        {
            InitializeComponent();
            network = new ActivationNetwork(new SigmoidFunction(), 9, 958);
            x = Resources.x;
            o = Resources.o;
            
            playerTurnB = false;

            comboBox1.SelectedIndex = 0;
            deactivatePanels();
            button1.Enabled = true;
            button3.Enabled = true;

        }

        public void rndFirstMove()
        {
            Random rnd = new Random();
            int spotX = rnd.Next(0, 9);

            spots[spotX] = 1;
            drawX(spotX);
        }

        public void computerTurn()
        {
            bool doneMove = false;
            double[] copiedSpots = new double[9];
            spots.CopyTo(copiedSpots, 0);

            for (int i=0; i < spots.Length; i++)
            {
                if (spots[i] == 0)
                {
                    copiedSpots[i] = 1;
                    if (isWin(copiedSpots) == 1)
                    {
                        doneMove = true;
                        spots[i] = 1;
                        drawX(i);
                        end(1);
                        return;
                    }
                    copiedSpots[i] = 0;
                }
                
            }

            if (doneMove == false)
            {
                for (int i = 0; i <spots.Length; i++)
                {
                    if (spots[i] == 0)
                    {
                        copiedSpots[i] = -1;
                        if (isWin(copiedSpots) == -1)
                        {
                            doneMove = true;
                            spots[i] = 1;
                            drawX(i);
                            playerTurnB = true;
                            return ;
                        }
                        copiedSpots[i] = 0;
                    }
                }
            }

            if (doneMove == false)
            {
                double[] weights = new double[9];
                for (int i = 0; i < 9; i++)
                {
                    if (spots[i] == 0)
                    {
                        copiedSpots[i] = 1;
                        double[] tmp = network.Compute(copiedSpots);
                        double suma = 0;

                        //wartosci przyjete dla planszy 3x3

                        for (int j = 0; j < 626; j++) 
                        {
                            suma += tmp[j];           
                        }
                        for (int j = 626; j < 958; j++)
                        {
                            suma -= tmp[i];
                        }
                        weights[i] = suma;            

                    }
                    else
                    {
                        continue;
                    }
                }
                double maxValue = weights.Max();
                int maxIndex = weights.ToList().IndexOf(maxValue);
                spots[maxIndex] = 1;
                drawX(maxIndex);
            }
            playerTurnB = true;

            int counter = 0;
            for (int i = 0; i < spots.Length; i++)
            {
                if (spots[i] != 0)
                {
                    counter++;
                }
                if (counter == spots.Length)
                {
                    end(2);
                }
            }
        }

        public void drawX(int index)
        {
            switch (index)
            {
                case 0:
                    {
                        panel1.BackgroundImage = x;
                        break;
                    }
                case 1:
                    {
                        panel2.BackgroundImage = x;
                        break;
                    }
                case 2:
                    {
                        panel3.BackgroundImage = x;
                        break;
                    }
                case 3:
                    {
                        panel4.BackgroundImage = x;
                        break;
                    }
                case 4:
                    {
                        panel5.BackgroundImage = x;
                        break;
                    }
                case 5:
                    {
                        panel6.BackgroundImage = x;
                        break;
                    }
                case 6:
                    {
                        panel7.BackgroundImage = x;
                        break;
                    }
                case 7:
                    {
                        panel8.BackgroundImage = x;
                        break;
                    }
                case 8:
                    {
                        panel9.BackgroundImage = x;
                        break;
                    }
            }
        }

        public void playerTurn(int index,object sender)
        {
            if (spots[index] != 0 || playerTurnB == false)
            {
                return;
            }
            playerTurnB = false;
            spots[index] = -1;
            ((Panel)sender).BackgroundImage = o;
            if (isWin(spots) == -1) { end(-1); }
            computerTurn();
        }

        private void panel1_MouseClick(object sender, MouseEventArgs e)
        {
            playerTurn(0,sender);
            
        }

        private void panel2_MouseClick(object sender, MouseEventArgs e)
        {
            playerTurn(1,sender);
        }

        private void panel3_MouseClick(object sender, MouseEventArgs e)
        {
            playerTurn(2, sender);
        }

        private void panel4_MouseClick(object sender, MouseEventArgs e)
        {
            playerTurn(3, sender);
        }

        private void panel5_MouseClick(object sender, MouseEventArgs e)
        {
            playerTurn(4, sender);
        }

        private void panel6_MouseClick(object sender, MouseEventArgs e)
        {
            playerTurn(5, sender);
        }

        private void panel7_MouseClick(object sender, MouseEventArgs e)
        {
            playerTurn(6, sender);
        }

        private void panel8_MouseClick(object sender, MouseEventArgs e)
        {
            playerTurn(7, sender);
        }

        private void panel9_MouseClick(object sender, MouseEventArgs e)
        {
            playerTurn(8, sender);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            loadingLearningData();
            switch (comboBox1.SelectedIndex)
            {
                case 0:
                    {
                        PerceptronLearning learning = new PerceptronLearning(network);
                        double error;
                        do
                        {
                            error = learning.RunEpoch(input, output);
                            System.Console.WriteLine(error);
                        } while (error > 0.1);
                        break;
                    }
                case 1:
                    {
                        BackPropagationLearning learning = new BackPropagationLearning(network);
                        double error;
                        do
                        {
                            error = learning.RunEpoch(input, output);
                            System.Console.WriteLine(error);
                        } while (error > 0.1);
                        break;
                    }
                case 2:
                    {
                        DeltaRuleLearning learning = new DeltaRuleLearning(network);
                        double error;
                        do
                        {
                            error = learning.RunEpoch(input, output);
                            System.Console.WriteLine(error);
                        } while (error > 0.1);
                        break;
                    }
            }
            activatePanels();
            button1.Enabled = false;
            button3.Enabled = false;
            rndFirstMove();
        }

        public int isWin(double[]spots)
        {
            if (spots[0] == -1 && spots[1] == -1 && spots[2] == -1 ||
                spots[3] == -1 && spots[4] == -1 && spots[5] == -1 ||
                spots[6] == -1 && spots[7] == -1 && spots[8] == -1 ||
                spots[0] == -1 && spots[3] == -1 && spots[6] == -1 ||
                spots[1] == -1 && spots[4] == -1 && spots[7] == -1 ||
                spots[2] == -1 && spots[5] == -1 && spots[8] == -1 ||
                spots[0] == -1 && spots[4] == -1 && spots[8] == -1 ||
                spots[2] == -1 && spots[4] == -1 && spots[6] == -1)
            {
                return -1;
            }
            else if (spots[0] == 1 && spots[1] == 1 && spots[2] == 1 ||
               spots[3] == 1 && spots[4] == 1 && spots[5] == 1 ||
               spots[6] == 1 && spots[7] == 1 && spots[8] == 1 ||
               spots[0] == 1 && spots[3] == 1 && spots[6] == 1 ||
               spots[1] == 1 && spots[4] == 1 && spots[7] == 1 ||
               spots[2] == 1 && spots[5] == 1 && spots[8] == 1 ||
               spots[0] == 1 && spots[4] == 1 && spots[8] == 1 ||
               spots[2] == 1 && spots[4] == 1 && spots[6] == 1)
            {
                return 1;
            }
            else if (spots[0] != 0 && spots[1] != 0 && spots[2] != 0 &&
                 spots[3] != 0 && spots[4] != 0 && spots[5] != 0 &&
                 spots[6] != 0 && spots[7] != 0 && spots[8] != 0)
            {
                return 2;
            }
            else
                return 0;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            restart();
        } 

        private void button3_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Network|*.network";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                network = (ActivationNetwork)Network.Load(dialog.FileName);
            }

            activatePanels();
            button1.Enabled = false;
            button3.Enabled = false;
            button4.Enabled = false;
            playerTurnB = true;
            rndFirstMove();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            deactivatePanels();
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.FileName = "neural_network.network";
            dialog.Filter = "Network|*.network";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                network.Save(dialog.FileName);
            }

            activatePanels();
        }

        public void restart()
        {
            panel1.BackgroundImage = null;
            panel2.BackgroundImage = null;
            panel3.BackgroundImage = null;
            panel4.BackgroundImage = null;
            panel5.BackgroundImage = null;
            panel6.BackgroundImage = null;
            panel7.BackgroundImage = null;
            panel8.BackgroundImage = null;
            panel9.BackgroundImage = null;
            for(int i = 0; i < spots.Length; i++)
            {
                spots[i] = 0;
            }
            deactivatePanels();
            button1.Enabled = true;
            button3.Enabled = true;
            button4.Enabled = true;
        }

        public void end(int winner)
        {
            if (winner == 1) {
                MessageBox.Show("LOSE!","Result");
                    }
            if (winner == 2)
            {
                MessageBox.Show("DRAW!", "Result");
            }
            if (winner == -1)
            {
                MessageBox.Show("WIN!", "Result");
            }
            restart();
        }

        public void deactivatePanels() {
            panel1.Enabled = false;
            panel2.Enabled = false;
            panel3.Enabled = false;
            panel4.Enabled = false;
            panel5.Enabled = false;
            panel6.Enabled = false;
            panel7.Enabled = false;
            panel8.Enabled = false;
            panel9.Enabled = false;
        }

        public void activatePanels()
        {
            panel1.Enabled = true;
            panel2.Enabled = true;
            panel3.Enabled = true;
            panel4.Enabled = true;
            panel5.Enabled = true;
            panel6.Enabled = true;
            panel7.Enabled = true;
            panel8.Enabled = true;
            panel9.Enabled = true;
        }

        public void loadingLearningData()
        {
            int lines = 0;

            OpenFileDialog fd = new OpenFileDialog();
            fd.Title = "Wybierz plik z danymi do uczenia";
            fd.Filter = "|*.txt";

            if (fd.ShowDialog() == DialogResult.OK)
            {
                StreamReader sr = new StreamReader(fd.FileName);
                while (sr.ReadLine() != null)
                {
                    lines++;
                }
                
                sr.BaseStream.Position = 0;
                input = new double[lines][];
                output = new double[lines][];

                for (int i = 0; i < lines; i++)
                {
                    input[i] = new double[9];
                    output[i] = new double[lines];

                    string tmp = sr.ReadLine();
                    string[] stmp = tmp.Split(',');

                    //wartosci przyjete dla planszy 3x3
                    for (int j = 0; j < 9; j++)
                    {
                        input[i][j] = Double.Parse(stmp[j]);
                    }
                    for (int j = 0; j < 626; j++) 
                        output[i][j] = 1;  
                }

            }
            else
            {
                MessageBox.Show("Zły plik", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}
